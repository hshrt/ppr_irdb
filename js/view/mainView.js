MainView = Backbone.View.extend({
    el: '#content_container',
    events: {
        'click #addRadio': 'addRadio',
        'click #searchRadio': 'searchRadio',
        'keyup #searchInput': 'keyPressEventHandler',
        'click #alertBoxClose': 'closeAlert',
        'click .viewSwitch': 'switchView',
        'click #refreshRadio': 'loadCurrentPage',
        'click #searchClear': 'clearSearch',
        'click #disableOffline': 'disableOffline'
    },
    render: function() {
        var self = this;
        $(this.el).empty();
        var template = _.template(tpl.get('main'));
        $(this.el).append(template());

        this.radioListView = new RadioListView();
        this.radioListView.setMainView(this);
        this.radioListView.render();
        this.radioItemView = new RadioItemView();

        this.presetListView = new PresetListView();
        this.presetListView.setMainView(this);
        this.presetListView.render();
        this.presetItemView = new PresetItemView();

        this.playerBarView = new PlayerBarView();
        this.playerBarView.render();

        this.switchView({currentTarget:{id:'main'}});
        this.closeAlert();

        return this;
    },
    destroy: function(){
        this.remove();
        this.unbind();
    },
    addRadio: function (event) {
        event.preventDefault();
        model = new Radio();
        this.radioItemView.render({mode: 'add', model: model});
    },
    editRadio: function (model) {
        this.radioItemView.render({mode: 'edit', model: model});
    },
    searchRadio: function (event) {
        if (event) {
            event.preventDefault();
        }
        $('#searchKeyword').text($('#searchInput').val());
        this.radioListView.fetchCollection();
    },
    keyPressEventHandler : function(event){
        if(event.keyCode == 13){
            this.searchRadio(event);
        }
    },
    closeAlert: function (event) {
        $('#alertBox').hide();
    },
    switchView: function (event) {
        if (event.currentTarget.id == 'main') {
            $('#presetView').hide();
            $('#mainDatabaseView').show();
            $('.viewSwitch#main').addClass('btn-primary');
            $('.viewSwitch#preset').removeClass('btn-primary');
        } else if (event.currentTarget.id == 'preset') {
            $('#mainDatabaseView').hide();
            $('#presetView').show();
            $('.viewSwitch#preset').addClass('btn-primary');
            $('.viewSwitch#main').removeClass('btn-primary');

        }
    },
    loadCurrentPage: function () {
        this.radioListView.loadCurrentPage();
    },
    clearSearch: function () {
        $('#searchInput').val('');
        this.searchRadio();
    },
    playRadio: function (name, url) {
        this.playerBarView.playRadio(name, url);
    },
    disableOffline: function () {
        var count = parseInt($('#disableOfflineCount').val());
        if (count > 0) {
            $.ajax({
                url: 'api/disable_offline',
                data: {count: count}
            }).done(function () {
                alert('Marked offline disabled!');
            });
        } else {
            alert('Enter offline count!');
        }
    }
});
