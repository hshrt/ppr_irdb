PresetListView = Backbone.View.extend({
    el : '#presetList',
    events: {
        'click a.editPreset': 'editPreset',
        'click a.playRadio': 'playRadio'
    },
    render : function() {
        this.fetchCollection();
        this.fillTable();
        return this;
    },
    fetchCollection: function () {
        var self = this;
        this.collection = new PresetCollection();
        this.collection.fetch({
            success: function (model, response, options) {
                self.fillTable();
            },
            error: function (model, response, options) {
                //TODO: Add error handling
            }
        });
    },
    fillTable: function () {
        $(this.el).empty();
        var template = _.template(tpl.get('presetList'));
        console.log(this.el);
        $(this.el).append(template(this.collection));
    },
    setMainView: function (v) {
        this.mainView = v;
    },
    playRadio: function (event) {
        this.mainView.playRadio(event.currentTarget.getAttribute('data-name'), event.currentTarget.getAttribute('data-url'));
    },
    editPreset: function (event) {
        var self = this;
        event.preventDefault();
        console.log(event);
        console.log(event.currentTarget.id);
        presetItem = new Preset({id: event.currentTarget.id});
        presetItem.fetch({
            success: function (model, response, options) {
                console.log(model);
                self.mainView.presetItemView.setListView(self);
                self.mainView.presetItemView.render({mode: 'edit', model: model});
            },
            error: function (model, response, options) {
                //TODO: Add error handling
            }
        });
    }
});