TesterView = Backbone.View.extend({
    el: "#content_container",
    events: {
        "click .btn-toggle": "toggleBtnClicked",
        "click #pauseTest": "pauseTest",
        "click #stopTest": "stopTest",
        "click #startTest": "startTest",
        "click #save-config": "saveConfig"
    },
    render: function () {
        $(this.el).empty();
        var template = _.template(tpl.get('tester'));
        $(this.el).append(template());
        $(this.el).data('backboneView', this);
        this.loadConfig(true);
        var x = this.loadConfig;
        this.updateTimer = setInterval(x, 5000);
    },
    destroy: function(){
        clearInterval(this.updateTimer);
        this.remove();
        this.unbind();
    },
    updateStatus: function () {
        var statusCollection = new TesterStatusCollection();
        statusCollection.fetch({
            success: function (model, response, options) {
                var total = 0, online = 0, offline = 0;
                model.forEach(function(element) {
                    total += parseInt(element.get('count'));
                    switch (parseInt(element.get('status'))) {
                        case 1:
                            online = parseInt(element.get('count'));
                            break;
                        case 2:
                            offline = parseInt(element.get('count'));
                            break;
                    }
                });

                var status = $('#config_status').val();
                if (status == 'RUNNING') {
                    $('#test-status-msg').text('Test in progress.');
                    $('#pauseTest').prop('disabled', false);
                    $('#stopTest').prop('disabled', false);
                    $('#startTest').prop('disabled', true);
                } else if (status == 'PAUSED(USER)') {
                    $('#test-status-msg').text('Test paused by user.');
                    $('#pauseTest').prop('disabled', false);
                    $('#stopTest').prop('disabled', true);
                    $('#startTest').prop('disabled', true);
                } else if (status == 'FINISHED') {
                    $('#test-status-msg').text('Test completed.');
                    $('#pauseTest').prop('disabled', true);
                    $('#stopTest').prop('disabled', true);
                    $('#startTest').prop('disabled', false);
                } else if (status == 'STOPPED') {
                    $('#test-status-msg').text('Test stopped by user.');
                    $('#pauseTest').prop('disabled', true);
                    $('#stopTest').prop('disabled', true);
                    $('#startTest').prop('disabled', false);
                }

                var statusPercent = (online + offline) / total * 100;
                var onlinePercent = online / total * 100;
                var offlinePercent = offline / total * 100;
                $('#statu-percent').text(statusPercent.toFixed(2));
                $('#status-total').text(total);
                $('#status-online').text(online);
                $('#status-offline').text(offline);
                $('#online-percent').width(onlinePercent + "%");
                $('#offline-percent').width(offlinePercent + "%");
            }
        });
    },
    loadConfig: function (first_time) {
        var self = $('#content_container').data('backboneView');
        this.testerConfig = new TesterConfigCollection();
        this.testerConfig.fetch({
            success: function (model, response, options) {
                model.forEach(function (element) {
                    var key = element.get('key');
                    var value = element.get('value');
                    if (key == 'status' || key == 'next_test' || first_time) {
                        console.log(key + ":" + value);

                        if ($('#config_' + key).length == 1) {
                            $('#config_' + key).val(value);
                        }

                        if (key == 'test_enable_only' || key == 'pause_if_no_internet') {
                            self.updateButtonState('#config_' + key, value);
                        }
                    }
                });
                self.updateStatus();
            }
        })
    },
    updateButtonState: function (btn_id, value) {
        if (value == '1') {
            $(btn_id + '_on').removeClass('btn-default');
            $(btn_id + '_on').addClass('btn-primary');
            $(btn_id + '_off').removeClass('btn-primary');
            $(btn_id + '_off').addClass('btn-default');
        } else {
            $(btn_id + '_on').removeClass('btn-primary');
            $(btn_id + '_on').addClass('btn-default');
            $(btn_id + '_off').removeClass('btn-default');
            $(btn_id + '_off').addClass('btn-primary');
        }
    },
    toggleBtnClicked: function (event) {
        event.preventDefault();
        console.log(event.currentTarget.id);
        if (event.currentTarget.id.endsWith('_on')) {
            var key = '#' + event.currentTarget.id.replace(/_on$/, '')
            $(key).val('1');
            this.updateButtonState(key, '1');
        } else if (event.currentTarget.id.endsWith('_off')) {
            var key = '#' + event.currentTarget.id.replace(/_off$/, '')
            $(key).val('0');
            this.updateButtonState(key, '0');
        }
    },
    saveConfig: function () {
        var config = {};
        var testerConfigCollection = new TesterConfigCollection();
        $('input').each(function (index, element) {
            var testerConfigModel = new TesterConfig();
            var key = element.id.replace(/^config_/, '');
            //console.log(element.value);
            config[key] = element.value;
            testerConfigModel.set(key, element.value);
            testerConfigCollection.add(testerConfigModel);
        });
        console.log(config);
        $.ajax({
            method: 'PUT',
            url: 'api/tester_config',
            data: JSON.stringify(config),
        }).done(function () {
            console.log('Config saved');
        });
    },
    pauseTest: function () {
        $('#pauseTest').prop('disabled', true);
        $('#stopTest').prop('disabled', true);
        $('#startTest').prop('disabled', true);
        $.ajax({
            url: 'api/tester/pause'
        });
    },
    stopTest: function () {
        $('#pauseTest').prop('disabled', true);
        $('#stopTest').prop('disabled', true);
        $('#startTest').prop('disabled', true);
        $.ajax({
            url: 'api/tester/stop'
        });
    },
    startTest: function () {
        $('#pauseTest').prop('disabled', true);
        $('#stopTest').prop('disabled', true);
        $('#startTest').prop('disabled', true);
        $.ajax({
            url: 'api/tester/start'
        });
    }
});