RadioListView = Backbone.View.extend({
    el : '#radioList',
    events: {
        'click a.playRadio': 'playRadio',
        'click a.editRadio': 'editRadio',
        'click a.deleteRadio': 'deleteRadio',
        'click a.toggleRadio': 'toggleRadio'
    },
    render : function() {
        var self = this;
        this.radioPaginator = new RadioPaginatorView();
        this.radioPaginator.setListView(self);
        this.fetchCollection();
        this.fillTable();
        return this;
    },
    fetchCollection: function () {
        var self = this;
        this.collection = new RadioCollection();
        this.collection.getFirstPage({
            success: function (model, response, options) {
                self.fillTable();
            },
            error: function (model, response, options) {
                //TODO: Add error handling
            }
        });
    },
    fillTable: function () {
        $(this.el).empty();
        var template = _.template(tpl.get('radioList'));
        console.log(this.el);
        $(this.el).append(template(this.collection));
        this.radioPaginator.render(this.collection);
    },
    loadPrevPage: function () {
        var self = this;
        if (this.collection.state.lastPage == null || this.collection.hasPreviousPage()) {
            this.collection.getPreviousPage()
                .done(function () {
                    self.fillTable();
                    self.radioPaginator.render(self.collection);
                })
                .fail(function () {
                    //TODO: Add error handling
                })
        }
    },
    loadNextPage: function () {
        var self = this;
        if (this.collection.state.lastPage == null || this.collection.hasNextPage()) {
            this.collection.getNextPage()
                .done(function (model, response, options) {
                    self.fillTable();
                    self.radioPaginator.render(self.collection);
                })
                .fail(function () {
                    //TODO: Add error handling
                });
        }
    },
    loadCurrentPage: function () {
        this.loadPage(this.collection.state.currentPage + 1);
    },
    loadPage: function (pageNumber) {
        console.log('loadPage: ' + pageNumber);
        var self = this;
        this.collection.getPage(pageNumber - 1)
            .done(function (model, response, options) {
                self.fillTable();
                self.radioPaginator.render(self.collection);
            })
            .fail(function() {
                //TODO: Add error handling
            });
    },
    setMainView: function (v) {
        this.mainView = v;
    },
    playRadio: function (event) {
        this.mainView.playRadio(event.currentTarget.getAttribute('data-name'), event.currentTarget.getAttribute('data-url'));
    },
    editRadio: function (event) {
        var self = this;
        event.preventDefault();
        console.log(event);
        console.log(event.currentTarget.id);
        radioItem = new Radio({id: event.currentTarget.id, myID: event.currentTarget.id});
        radioItem.fetch({
            success: function (model, response, options) {
                console.log(model);
                self.mainView.radioItemView.render({mode: 'edit', model: model});
            },
            error: function (model, response, options) {
                //TODO: Add error handling
            }
        });
    },
    deleteRadio: function (event) {
        var self = this;
        event.preventDefault();
        console.log(event);
        console.log(event.currentTarget.id);
        radioItem = new Radio({id: event.currentTarget.id, myID: event.currentTarget.id});
        radioItem.fetch({
            success: function (model, response, options) {
                console.log(model);
                self.mainView.radioItemView.render({mode: 'delete', model: model});
            },
            error: function (model, response, options) {
                //TODO: Add error handling
            }
        });
    },
    toggleRadio: function (event) {
        event.preventDefault();
        console.log(this.collection.get(event.currentTarget.id));
        var self = this;
        var target = event.currentTarget;
        var radio = this.collection.get(event.currentTarget.id);
        if (radio.get('isEnable') == '1') {
            radio.set('isEnable', 0);
        } else {
            radio.set('isEnable', 1);
        }
        radio.save({}, {
            success: function () {
                self.fillTable();
            },
            error: function (model, request) {
            }
        });
    }
});