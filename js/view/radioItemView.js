RadioItemView = Backbone.View.extend({
    el : '#modalContainer',
    events: {
        'click #save': 'saveRadio',
        'click #delete': 'deleteRadio'
    },
    render : function(options) {
        this.model = options.model;
        console.log(options);
        $(this.el).empty();
        var template = _.template(tpl.get('radioItem'));
        $(this.el).append(template(options));
        $("#myModal").modal();
        $("#itemAlertBox").hide();
    },
    saveRadio: function() {
        this.model.set({
            radioName: $('#item-radioName').val(),
            radioURL: $('#item-radioURL').val(),
            myGenre: $('#item-myGenre').val(),
            myCountry: $('#item-myCountry').val(),
            myCity: $('#item-myCity').val(),
            radioFormat: $('#item-radioFormat').val(),
            myBitrate: $('#item-myBitrate').val()
        });
        if (this.model.isValid()) {
            this.model.save({}, {
                success: function () {
                    $("#alertMessage").text("Saved successfully");
                    $("#alertBox").removeClass("alert-danger");
                    $("#alertBox").addClass("alert-success");
                    $("#alertBox").show();
                    $("#myModal").find("button.close").click();
                    $("#refreshRadio").click();
                    window.scroll(0, 0);
                },
                error: function (model, request) {
                    console.log(request);
                    $("#itemAlertMessage").text("Error: " + request.responseJSON.msg);
                    $("#itemAlertBox").show();
                    window.scroll(0, 0);
                }
            });
        } else {
            $("#itemAlertMessage").text(this.model.validationError);
            $("#itemAlertBox").show();
        }
    },
    deleteRadio: function () {
        this.model.destroy({
            success: function () {
                $("#alertMessage").text("Deleted successfully");
                $("#alertBox").removeClass("alert-danger");
                $("#alertBox").addClass("alert-success");
                $("#alertBox").show();
                $("#myModal").find("button.close").click();
                $("#refreshRadio").click();
                window.scroll(0, 0);

            },
            error: function (model, request) {
                console.log(request);
                $("#itemAlertMessage").text("Error: " + request.responseJSON.msg);
                $("#itemAlertBox").show();
                window.scroll(0, 0);
            }
        })
    }
});