PresetItemView = Backbone.View.extend({
    el : '#modalContainer',
    events: {
        'click #savePreset': 'savePreset'
    },
    setListView: function (listView) {
        this.listView = listView;
    },
    render : function(options) {
        this.model = options.model;
        console.log(options);
        $(this.el).empty();
        var template = _.template(tpl.get('presetItem'));
        $(this.el).append(template(options));
        $("#myModal").modal();
        $("#itemAlertBox").hide();
    },
    savePreset: function() {
        this.model.set({
            name: $('#item-radioName').val(),
            url: $('#item-radioURL').val(),
        });
        if (this.model.isValid()) {
            self = this;
            this.model.save({}, {
                success: function () {
                    $("#alertMessage").text("Saved successfully");
                    $("#alertBox").removeClass("alert-danger");
                    $("#alertBox").addClass("alert-success");
                    $("#alertBox").show();
                    $("#myModal").find("button.close").click();
                    self.listView.render();
                },
                error: function (model, request) {
                    console.log(request);
                    $("#itemAlertMessage").text("Error: " + request.responseJSON.msg);
                    $("#itemAlertBox").show();
                }
            });
        } else {
            $("#itemAlertMessage").text(this.model.validationError);
            $("#itemAlertBox").show();
        }
    }
});