RadioPaginatorView = Backbone.View.extend({
    el: '#radioPaginator',
    events: {
        "click #prev": "prevClicked",
        "click #next": "nextClicked",
        "keyup #pageNumber": "keyPressEventHandler"
    },
    setListView: function (listView) {
        this.listView = listView;
    },
    render: function(options) {
        var self = this;
        console.log(options);
        $(this.el).empty();
        var template = _.template(tpl.get('radioPaginator'));
        $(this.el).append(template(options));
        return this;
    },
    prevClicked: function (event) {
        event.preventDefault();
        this.listView.loadPrevPage();
    },
    nextClicked: function (event) {
        event.preventDefault();
        this.listView.loadNextPage();
    },
    keyPressEventHandler : function(event){
        event.preventDefault();
        if(event.keyCode == 13){
            totalPage = this.listView.collection.state.totalPages;
            pageNumber = parseInt($('#pageNumber').val());
            if (Number.isInteger(pageNumber) && pageNumber > 0 && pageNumber <= totalPage) {
                this.listView.loadPage(pageNumber);
            }
        }
    }
});
