PlayerBarView = Backbone.View.extend({
    el: '#playerBar',
    events: {
        'click #addRadio': 'addRadio',
        'click #stopRadio': 'stopRadio'
    },
    render: function() {
        $(this.el).hide();
    },
    playRadio: function (name, url) {
        $(this.el).show();
        $('#playerBarName').text(name);
        var videoTag = $('#player').get(0);
        videoTag.src = url;
        videoTag.load();
        videoTag.play();
    },
    stopRadio: function (event) {
        $(this.el).hide();
        var videoTag = $('#player').get(0);
        videoTag.src = '';
        videoTag.load();
    }
});