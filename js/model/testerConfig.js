TesterConfig = Backbone.Model.extend({
    defaults: {
        "key": "",
        "value": ""
    },
    parse: function(response) {
        if (response.data) {
            // Coming from api directly
            return response.data[0];
        } else {
            // Coming from collection item
            return response;
        }
    }
});