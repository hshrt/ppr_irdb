Preset = Backbone.Model.extend({
    defaults: {
        "id": "",
        "name": "",
        "url": ""
    },
    validate: function (attrs, options) {
        if (!attrs.name) {
            return 'Station Name required';
        }
        if (!attrs.url) {
            return 'URL required';
        }
    },
    url: function () {
        return 'api/preset/' + this.id;
    },
    parse: function(response) {
        if (response.data) {
            // Coming from api directly
            return response.data[0];
        } else {
            // Coming from collection item
            return response;
        }
    }
});