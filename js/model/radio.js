Radio = Backbone.Model.extend({
    idAttribute: 'myID',
    defaults: {
        "myID": "",
        "radioName": "",
        "radioURL": "",
        "myCountry": "",
        "myCity": "",
        "myGenre": "",
        "myLocation": "",
        "myBitrate": "",
        "radioFormat": "",
        "isEnable": "1"
    },
    validate: function (attrs, options) {
        if (!attrs.radioName) {
            return 'Station Name required';
        }
        if (!attrs.radioURL) {
            return 'URL required';
        }
        if (!attrs.myCountry) {
            return 'Country required';
        }
        if (!attrs.myCity) {
            return 'City required';
        }
        if (!attrs.myGenre) {
            return 'Genre required';
        }
        if (!attrs.myBitrate) {
            return 'Bitrate required';
        }
        if (!attrs.radioFormat) {
            return 'Format required';
        }
    },
    url: function () {
        return 'api/radio/' + this.id;
    },
    parse: function(response) {
        if (response.data) {
            // Coming from api directly
            return response.data[0];
        } else {
            // Coming from collection item
            return response;
        }
    }
});