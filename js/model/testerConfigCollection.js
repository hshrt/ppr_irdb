TesterConfigCollection = Backbone.Collection.extend({
    model: TesterConfig,
    url: function () {
        return 'api/tester_config';
    },
    parse: function(response) {
        return response.data;
    }
});