RadioCollection = Backbone.PageableCollection.extend({
    model: Radio,
    url: 'api/radio/',
    mode: 'server',
    state: {
        firstPage: 0,
        currentPage: 0
    },
    queryParams: {
        search: function () {
            var searchText = $('#searchKeyword').text();
            if (searchText) {
                return searchText;
            } else {
                return null;
            }
        },
        isEnable: function () {
            var isEnable = $('#searchIsEnable').val();
            if (isEnable == '1') {
                return '1';
            } else if (isEnable == '0') {
                return '0';
            } else {
                return null;
            }
        },
        isOnline: function () {
            var isOnline = $('#searchIsOnline').val();
            if (isOnline == '1') {
                return '1';
            } else if (isOnline == '0') {
                return '0';
            } else {
                return null;
            }
        }
    },
    parse: function(response) {
        this.state.totalRecords = parseInt(response.meta.row_count);
        this.state.totalPages = Math.ceil(this.state.totalRecords / this.state.pageSize);
        return response.data;
    }
});
