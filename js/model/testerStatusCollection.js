TesterStatusCollection = Backbone.Collection.extend({
    model: TesterStatus,
    url: function () {
        return 'api/tester_status';
    },
    parse: function(response) {
        return response.data;
    }
});