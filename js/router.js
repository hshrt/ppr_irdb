// Router is responsible for driving the application. Usually
// this means populating the necessary data into models and
// collections, and then passing those to be displayed by
// appropriate views.
var Router = Backbone.Router.extend({

    initialize : function() {
        this.mainView = null;

    },

    routes: {
        '': 'index',  // At first we display the index route
        'tester': 'tester'
    },

    index: function() {
        this.destroy();
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        console.log('route index');
        this.mainView = new MainView();
        this.mainView.render();
    },
    tester: function() {
        this.destroy();
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        console.log('route tester');
        this.testerView = new TesterView();
        this.testerView.render();
    },
    destroy: function () {
        if (this.mainView) {
            this.mainView.destroy();
        }
        if (this.testerView) {
            this.testerView.destroy();
        }
        if ($("#content_container").length == 0) {
            $('body').append('<div id="content_container"></div>');
        }
    }
});
