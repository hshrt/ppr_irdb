<?php
/**
 * Created by PhpStorm.
 * User: johnau
 * Date: 25/09/2015
 * Time: 10:30
 */
include_once('db.inc');
$error = false;
$userid = 0;
$username = '';
$isadmin = 0;

if (isset($_POST['login'])) {
    // Validate login
    if (!isset($_POST['username']) || !isset($_POST['password']) ||
        $_POST['username'] == "" || $_POST['password'] == "") {
        $msg = "Invalid username/password";
        $error = true;
    }

    if (!$error) {
        $result = db_get_user_by_username($_POST['username']);
        if ($result['response'] == 'ok') {

            $userid = $result['data'][0]->id;
            $username = $result['data'][0]->username;
            $isadmin = $result['data'][0]->isadmin;

            // Check password
            $password = $result['data'][0]->password;
            if (!password_verify($_POST['password'], $password)) {
                $msg = "Invalid username/password";
                $error = true;
            }



        } else {
            $error = true;
            $msg = $result['msg'];
        }
    }
    // Login success
    if (!$error) {
        session_start();
        session_regenerate_id(true);

        $_SESSION['userid'] = $userid;
        $_SESSION['username'] = $username;
        $_SESSION['isadmin'] = ($isadmin == 1);
        $path = $_SERVER['REQUEST_URI'];
        $path = substr($path, 0, strrpos($path, '/'));
        $path = substr($path, strrpos($path, '/') + 1);
        $_SESSION['apppath'] = $path;
        header("Location: index.php");
        die;
    }
} else {
    // Destroy session
    session_start();
    session_unset();
    session_destroy();
}


?>
<html>
<head>
    <title>Sign In</title>
</head>
<body>

<!-- Include js and css -->
<link href="3rdparty/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="3rdparty/js/jquery-1.11.2.min.js"></script>
<script src="3rdparty/js/bootstrap.min.js"></script>

<div class="container" style="margin-top: 20px">
    <div class="panel panel-primary">
        <div class="panel-heading">Sign In</div>
        <div class="panel-body">
<?php if (isset($msg)) { ?>
            <div class="row">
                <div class="col-md-12 alert-danger"><?= $msg ?></div>
            </div>
<?php } ?>
            <form name="login" action="login.php" method="POST">
                <div class="row">
                    <div class="col-md-2">Username</div>
                    <div class="col-md-4"><input type="text" class="form-control" name="username"></div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-2">Password</div>
                    <div class="col-md-4"><input type="password" class="form-control" name="password"></div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-10">
                        <button class="btn btn-primary" name="login" type="submit">Ok</button>
                        <button class="btn">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>