<?php
/**
 * Created by PhpStorm.
 * User: johnau
 * Date: 10/26/2017
 * Time: 5:05 PM
 */

session_start();
$path = $_SERVER['REQUEST_URI'];
$path = substr($path, 0, strrpos($path, '/'));
$path = substr($path, strrpos($path, '/') + 1);
if (!isset($_SESSION['userid']) ||
    !isset($_SESSION['apppath']) ||
    strcmp($path, $_SESSION['apppath']) != 0) {
    header("Location: login.php");
    die;
}

?>
<html>
<head>
</head>
<body>

<link href="3rdparty/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="3rdparty/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<script src="3rdparty/js/jquery-1.11.2.min.js"></script>
<script src="3rdparty/js/bootstrap.min.js"></script>
<script src="3rdparty/js/underscore-min.js"></script>
<script src="3rdparty/js/backbone-min.js"></script>
<script src="3rdparty/js/moment.min.js"></script>
<script src="3rdparty/js/bootstrap-datetimepicker.min.js"></script>
<script src="3rdparty/js/backbone.paginator.min.js"></script>

<script src="js/router.js"></script>
<script src="js/tpl.js"></script>
<script src="js/model/radio.js"></script>
<script src="js/model/radioCollection.js"></script>
<script src="js/model/preset.js"></script>
<script src="js/model/presetCollection.js"></script>
<script src="js/model/testerStatus.js"></script>
<script src="js/model/testerStatusCollection.js"></script>
<script src="js/model/testerConfig.js"></script>
<script src="js/model/testerConfigCollection.js"></script>

<script src="js/view/mainView.js"></script>
<script src="js/view/radioListView.js"></script>
<script src="js/view/radioPaginatorView.js"></script>
<script src="js/view/radioItemView.js"></script>
<script src="js/view/presetListView.js"></script>
<script src="js/view/presetItemView.js"></script>
<script src="js/view/playerBarView.js"></script>
<script src="js/view/testerView.js"></script>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-brand">Internet Radio Database</div>
        <ul class="nav navbar-nav">
            <li><a href="#">Database</a></li>
            <li><a href="#tester">Auto Tester</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="">User: <?= $_SESSION['username'] ?></a></li>
            <li><a href="login.php">Logout</a></li>
        </ul>
    </div>
</nav>

<div id="content_container">
</div>

<script>
    var router = new Router();
    jQuery(document).ready(function() {
        // When the document is ready we instantiate the router
        tpl.loadTemplates(['main', 'radioList', 'radioPaginator', 'radioItem', 'presetList', 'presetItem', 'tester'], function () {
            Backbone.history.start();
        });
    });
</script>

</body>
</html>